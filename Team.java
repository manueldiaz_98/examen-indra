package prueba;

import java.util.Optional;

public class Team {
	
	private Integer id;
	private String estadio;
	private Optional<Entrenador> entrenador;
	
	
	public Team() {
		entrenador = Optional.empty();
	}
	
	public Team(Integer id, String estadio) {
		this.id= id;
		this.estadio = estadio;
		
		Entrenador e = new Entrenador("Guardiola");
		this.entrenador = Optional.of(e);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEstadio() {
		return estadio;
	}

	public void setEstadio(String estadio) {
		this.estadio = estadio;
	}

	public Optional<Entrenador> getEntrenador() {
		return entrenador;
	}

	/*public void setEntrenador(Optional<Entrenador> entrenador) {
		this.entrenador = entrenador;
	}*/
}
