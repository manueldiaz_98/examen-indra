package time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

public class AppTime {

	public static void main(String[] args) {
		fechaLocalDateDiaAno();
		horaLocalTimeNano();
		obtenerFechaHoraLocalDateTime();
		formateoFecha();
	}

	public static void fechaLocalDateDiaAno() {
		LocalDate hoy = LocalDate.now();
		
		System.out.println(hoy + " Dia del a�o " + hoy.getDayOfYear());
	}
	
	public static void horaLocalTimeNano() {
		LocalTime ahora = LocalTime.now();
		
		System.out.println(ahora + " NanoSegundos " + ahora.getNano());
	}
	
	public static void formateoFecha() {
			String fechaFormateada = "16/11/2021";
			
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			Date paraFormatear = null;
				 try {
					paraFormatear = formato.parse(fechaFormateada);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
				 System.out.println("Parsed date: " + paraFormatear);
				 
			
		
	}
	
	public static void obtenerFechaHoraLocalDateTime() {
		LocalDateTime hoyAhora = LocalDateTime.now();
		
		System.out.println("Fecha: " + hoyAhora.getDayOfMonth() + " " + hoyAhora.getMonth() + " " + hoyAhora.getYear() 
		+ " Hora: " + hoyAhora.getHour() +":" + hoyAhora.getMinute());
	}
}	
