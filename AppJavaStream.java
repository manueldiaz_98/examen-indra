package domain;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


public class AppJavaStream {

	public static void main(String[] args) {
		Client c1 = new Client(1, "C1", "D1", "T1");
		Client c2 = new Client(2, "C2", "D2", "T2");
		Client c3 = new Client(3, "C3", "D3", "T3");

		List<Bill> bills = new ArrayList<Bill>();
		Bill f1 = new Bill(1, "FAC-1", c1, 100D);
		Bill f2 = new Bill(2, "FAC-2", c2, 200D);
		Bill f3 = new Bill(3, "FAC-3", c3, 300D);

		bills.add(f1);
		bills.add(f2);
		bills.add(f3);
		bills.add(new Bill(4, "FAC-4", c1, 125D));
		bills.add(new Bill(8, "FAC-8", c2, 200D));
		bills.add(new Bill(9, "FAC-9", c3, 250D));
		
		//forEachLambda(bills);
		//concatenaFiltros(bills);
		//recorrerIterator(bills);
		//comparatorMaxMin(bills);
		elementosDiferentes(bills);
	}

	private static void forEachLambda(List<Bill> listaBill) {
		listaBill.forEach(bill -> System.out.println(bill));
	}
	
	private static void concatenaFiltros(List<Bill> listaBill) {
		Stream<Bill> streamBill = listaBill.stream();
		
		streamBill.filter(bill -> bill.getTotal() > 100).filter(bill -> bill.getTotal() < 250).
		forEach(bill -> System.out.println(bill));
	}
	
	private static void recorrerIterator(List<Bill> listaBill) {
		Stream<Bill> streamBill = listaBill.stream();
		
		Iterator<Bill> it  = streamBill.filter(singleBill -> {System.out.println("1:"+ singleBill);
		return singleBill.getTotal() > 130;
		}).iterator();
		
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
	
	private static void comparatorMaxMin(List<Bill> listaBill) {
		Stream<Bill> streamBill1 = listaBill.stream();
		Stream<Bill> streamBill2 = listaBill.stream();
		
		Comparator<Bill> comparatorMax = new Comparator<Bill>() {
			public int compare(Bill bill1, Bill bill2) {
				return bill1.getTotal().intValue() - bill2.getTotal().intValue();
			}
		};
		
		Optional<Bill> billMax = streamBill1.max(comparatorMax);
		if(billMax.isPresent()) {
			System.out.println(billMax);
		}
		
		Optional<Bill> billMin = streamBill2.min(comparatorMax);
		if(billMin.isPresent()) {
			System.out.println(billMin);
		}
		
	}
	
	private static void elementosDiferentes(List<Bill> listaBill) {
		Stream<Bill> streamBill1 = listaBill.stream();
		
		streamBill1.distinct().forEach(bill -> System.out.println(bill));
	}
}
