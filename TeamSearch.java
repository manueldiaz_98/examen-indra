package prueba;
import java.util.Optional;

public class TeamSearch {
	public Team search(Integer id) {
		
		switch(id) {
		 
		case 1 : return new Team(1, "Barcelona FC");
		case 2 : return new Team(2, "Real Madrid");
		case 3 : return new Team(3, "Real Betis");
		default: return null;
		
		}
	}
	
	public Optional<Team>searchOptional(Integer id) {
		
		switch(id) {
		 
		case 1 : return Optional.of(new Team(1, "Barcelona FC"));
		case 2 : return Optional.of(new Team(2, "Real Madrid"));
		case 3 : return Optional.of(new Team(3, "Real Betis"));
		default: return Optional.empty();
		
		}
	}
}
