package prueba;
import java.util.Optional;

public class Entrenador {
	private String name;
	private Optional<Address> address;
	
	
	public Entrenador() {
		address = Optional.empty();
	}


	public Entrenador(String name) {
		this.name = name;
		 address = Optional.of(new Address("Donosti", "Calle"));
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Optional<Address> getAddress() {
		return address;
	}


	/*public void setAddress(Optional<Address> address) {
		this.address = address;
	}*/
	
	
}
