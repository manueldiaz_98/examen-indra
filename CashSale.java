package domain;

public class CashSale {

	private String billId;
	private Double total;

	public CashSale(String billId, Double total) {
		this.billId = billId;
		this.total = total;
	}

	public String getBillId() {
		return this.billId;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public Double getTotal() {
		return this.total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "Bill [billId=" + billId + ", total=" + total + "]";
	}

}
