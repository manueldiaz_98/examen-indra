package prueba;

import java.util.Optional;

public class AppOptional {

	public static TeamSearch teamSearch;
	
	public static void main(String[] args) {
		
		teamSearch = new TeamSearch();

		searchTeamUsingNestedOptional();
		
		searchTeamWithLambda();
		
	}

	//Búsqueda con anidamiento de objetos
	public static void searchTeamUsingNestedOptional() {
		Optional<Team> optionalTeam = teamSearch.searchOptional(1);
		
		if(optionalTeam.isPresent()) {
			Optional<Entrenador> dOp = optionalTeam.get().getEntrenador();
			if(dOp.isPresent()) {
				Optional<Address> optionalAddress = dOp.get().getAddress();
				if(optionalAddress.isPresent()) {
					System.out.println(optionalAddress.get().getCity());
				}
			}
		}
	}
	
	//Búsqueda lambda con optional
	public static void searchTeamWithLambda() {
		Optional<Team> optionalTeam = teamSearch.searchOptional(2);
		
		optionalTeam.ifPresent(foundEstadio -> System.out.println("Equipo: " + foundEstadio.getEstadio()));
	}
}
